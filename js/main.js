var alphaDust = function () {

    var _menuOn = false;

    function initPostHeader() {
        $('.main .post').each(function () {
            var $post = $(this);
            var $header = $post.find('.post-header.index');
            var $title = $post.find('h1.title');
            var $readMoreLink = $post.find('a.read-more');

            var toggleHoverClass = function () {
                $header.toggleClass('hover');
            };

            $title.hover(toggleHoverClass, toggleHoverClass);
            $readMoreLink.hover(toggleHoverClass, toggleHoverClass);
        });
    }

    function _menuShow () {
        $('nav a').addClass('menu-active');
        $('.menu-bg').show();
        $('.menu-item').css({opacity: 0});
        TweenLite.to('.menu-container', 1, {padding: '0 40px'});
        TweenLite.to('.menu-bg', 1, {opacity: '0.92'});
        TweenMax.staggerTo('.menu-item', 0.5, {opacity: 1}, 0.3);
        _menuOn = true;

        $('.menu-bg').hover(function () {
            $('nav a').toggleClass('menu-close-hover');
        });
    }

    function _menuHide() {
        $('nav a').removeClass('menu-active');
        TweenLite.to('.menu-bg', 0.5, {opacity: '0', onComplete: function () {
            $('.menu-bg').hide();
        }});
        TweenLite.to('.menu-container', 0.5, {padding: '0 100px'});
        $('.menu-item').css({opacity: 0});
        _menuOn = false;
    }

    function initMenu() {

        $('nav a').click(function () {
            if(_menuOn) {
                _menuHide();
            } else {
                _menuShow();
            }
        });

        $('.menu-bg').click(function (e) {
            if(_menuOn && e.target === this) {
                _menuHide();
            }
        });
    }

    function displayArchives() {
        $('.archive-post').css({opacity: 0});
        TweenMax.staggerTo('.archive-post', 0.4, {opacity: 1}, 0.15);
    }

    // make toc stay in the visible area
    function tocFixed() {
        var HEADER_OFFSET = 20;
        var $toc = $('#post-toc');
        if ($toc.length) {
            var minScrollTop = $toc.offset().top;
            $(window).scroll(function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop < minScrollTop) {
                    $toc.css({'position': 'absolute', 'top': minScrollTop - 70});
                } else {
                    $toc.css({'position': 'fixed', 'top': HEADER_OFFSET + 'px'});
                }
            });
        }
    }

    // current toc follows the content when scrolling
    function tocActive () {
        var HEADER_OFFSET = 30;
        var $toclink = $('.toc-link');
        var $headerlink = $('.headerlink');

        var headerlinkTop = $.map($headerlink, function (link) {
            return $(link).offset().top;
        });
        $(window).scroll(function () {
            var scrollTop = $(window).scrollTop();
            for (var i = 0; i < $toclink.length; i++) {
                var currentHeaderTop = headerlinkTop[i] - HEADER_OFFSET,
                    nextHeaderTop = i + 1 === $toclink.length ? Infinity : headerlinkTop[i + 1] - HEADER_OFFSET;

                if (currentHeaderTop < scrollTop && scrollTop <= nextHeaderTop) {
                    $($toclink[i]).addClass('active');
                } else {
                    $($toclink[i]).removeClass('active');
                }
            }
        });
    }

    // back to top
    function backToTop () {
        var $backToTop = $('#back-to-top');

        $backToTop.click(function () {
            console.log('click');
            $('html,body').animate({ scrollTop: 0 });
        });
    }

    // mobile nav toggle
    function mobileNavToggle () {
        var $mobileNav = $('.mobile-nav-icon'),
            $mobileMenu = $('.mobile-menu');

        $mobileNav.click(function () {
            if (!$mobileMenu.hasClass('show-menu')) {
                $mobileMenu.addClass('show-menu');
                $mobileMenu.removeClass('hide-menu');
                $mobileNav.addClass('show-menu').removeClass('hide-menu');
            } else {
                $mobileMenu.addClass('hide-menu');
                $mobileMenu.removeClass('show-menu');
                $mobileNav.removeClass('show-menu').addClass('hide-menu');
            }
        })
    }

    return {
        initPostHeader: initPostHeader,
        initMenu: initMenu,
        displayArchives: displayArchives,
        tocFixed: tocFixed,
        tocActive: tocActive,
        backToTop: backToTop,
        mobileNavToggle: mobileNavToggle
    };
}();


$(document).ready(function () {
    alphaDust.initPostHeader();
    alphaDust.initMenu();
    alphaDust.displayArchives();
    alphaDust.tocFixed();
    alphaDust.tocActive();
    alphaDust.backToTop();
    alphaDust.mobileNavToggle();
});